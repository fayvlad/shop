<?php
// HTTP
define('HTTP_SERVER', 'http://shop.fay.pp.ua/admin/');
define('HTTP_CATALOG', 'http://shop.fay.pp.ua/');

// HTTPS
define('HTTPS_SERVER', 'http://shop.fay.pp.ua/admin/');
define('HTTPS_CATALOG', 'http://shop.fay.pp.ua/');

// DIR
define('DIR_APPLICATION', '/var/www/shop/admin/');
define('DIR_SYSTEM', '/var/www/shop/system/');
define('DIR_LANGUAGE', '/var/www/shop/admin/language/');
define('DIR_TEMPLATE', '/var/www/shop/admin/view/template/');
define('DIR_CONFIG', '/var/www/shop/system/config/');
define('DIR_IMAGE', '/var/www/shop/image/');
define('DIR_CACHE', '/var/www/shop/system/cache/');
define('DIR_DOWNLOAD', '/var/www/shop/system/download/');
define('DIR_UPLOAD', '/var/www/shop/system/upload/');
define('DIR_LOGS', '/var/www/shop/system/logs/');
define('DIR_MODIFICATION', '/var/www/shop/system/modification/');
define('DIR_CATALOG', '/var/www/shop/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'shop');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
